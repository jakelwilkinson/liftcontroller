#include <Wire.h>
#include "Adafruit_MCP23017.h"

#define PIN_STATUS_LED 16

#define MCP_PIN_BUZZER 5 // buzzer on mcp
#define MCP_PIN_LATCH 13

#define MCP_INPUT_ESTOP 3
#define MCP_INPUT_UP 0
#define MCP_INPUT_DOWN 1
#define MCP_INPUT_ALARM 2

#define MCP_INPUT_GATE 8
uint8_t gateIsClosed = false;
#define MCP_INPUT_LIFT_MIN 9
#define MCP_INPUT_LIFT_MAX 10
#define MCP_INPUT_RAMP_MIN 11
#define MCP_INPUT_RAMP_MAX 12

#define MOTOR1_INH_MCP 7
#define MOTOR1_IN1 9
#define MOTOR1_IN2 10

#define MOTOR2_INH_MCP 6
#define MOTOR2_IN1 5
#define MOTOR2_IN2 6

int motor1_speed = 0; //-255 full reverse, 0 stop, 255 full forward
int motor1_target = 0;
int motor2_speed = 0; //-255 full reverse, 0 stop, 255 full forward
int motor2_target = 0;

Adafruit_MCP23017 mcp;

#define MODE_IDLE_LOW 0
#define MODE_IDLE_HIGH 1
#define MODE_MOVING_UP 2
#define MODE_MOVING_DOWN 3

#define ST_RAISING_RAMP 0
#define ST_LOWERING_RAMP 1
#define ST_RAISING_LIFT 2
#define ST_LOWERING_LIFT 3
#define ST_IDLE 4

long subtaskStartTime = 0;
long subtaskTimeout = 30000; // 30 second timeout for every action

int flashCount = 1;
int currentFlashCount = 0;
long lastFlash = 0;
long longFlashInterval = 3000;
long shortFlashInterval = 300;

bool inTestMode = false;

int temp = 0;

void setup()
{
  InitComms();
  LoadSettings();
  mcp.begin(); // use default address 0

  pinMode(PIN_STATUS_LED, OUTPUT);

  pinMode(MOTOR1_IN1, OUTPUT);
  pinMode(MOTOR1_IN2, OUTPUT);
  mcp.pinMode(MOTOR1_INH_MCP, OUTPUT);
  mcp.digitalWrite(MOTOR1_INH_MCP, LOW);

  pinMode(MOTOR2_IN1, OUTPUT);
  pinMode(MOTOR2_IN2, OUTPUT);
  mcp.pinMode(MOTOR2_INH_MCP, OUTPUT);
  mcp.digitalWrite(MOTOR2_INH_MCP, LOW);

  mcp.pinMode(MCP_PIN_BUZZER, OUTPUT);
  mcp.pinMode(MCP_PIN_LATCH, OUTPUT);
  SetLatchOpen(true);

  mcp.pinMode(MCP_INPUT_ESTOP, INPUT);
  mcp.pinMode(MCP_INPUT_UP, INPUT);
  mcp.pinMode(MCP_INPUT_DOWN, INPUT);
  mcp.pinMode(MCP_INPUT_ALARM, INPUT);
  mcp.pinMode(MCP_INPUT_GATE, INPUT);
  mcp.pinMode(MCP_INPUT_LIFT_MIN, INPUT);
  mcp.pinMode(MCP_INPUT_LIFT_MAX, INPUT);
  mcp.pinMode(MCP_INPUT_RAMP_MIN, INPUT);
  mcp.pinMode(MCP_INPUT_RAMP_MAX, INPUT);
}

void loop()
{
  CheckComms();

  FlashIndicator();

  if (!inTestMode)
  {
    CheckInputs();

    UpdateMotorControl();

    DoTasks();
  }

  delay(10);
}

void CheckInputs()
{
  // if up or down button pushed and not already doing so or in that position, begin the action
  if (ButtonStateINPUT_UP())
  {
    if (!IsGoingUp() && !ButtonStateLIFT_MAX())
    {
      StartActionGoUp();
    }
  }
  if (ButtonStateINPUT_DOWN())
  {
    if (!IsGoingDown() && !ButtonStateLIFT_MIN())
    {
      StartActionGoDown();
    }
  }

  gateIsClosed = ButtonStateGATE();
  if (!gateIsClosed)
  {
    if (IsGoingUp() || IsGoingDown())
    {
      EmergencyStop();
    }
  }

  if (ButtonStateEstop())
  {
    EmergencyStop();
  }

  if (ButtonStateINPUT_ALARM())
  {
    SetBuzzerActive(true);
  }
  else
  {
    SetBuzzerActive(false);
  }
}

bool ButtonStateEstop()
{
  temp = mcp.digitalRead(MCP_INPUT_ESTOP);
  return temp ^= IsInvertedESTOP();
}

bool ButtonStateINPUT_UP()
{
  temp = mcp.digitalRead(MCP_INPUT_UP);
  return temp ^= IsInvertedINPUT_UP();
}

bool ButtonStateINPUT_DOWN()
{
  temp = mcp.digitalRead(MCP_INPUT_DOWN);
  return temp ^= IsInvertedINPUT_DOWN();
}

bool ButtonStateINPUT_ALARM()
{
  temp = mcp.digitalRead(MCP_INPUT_ALARM);
  return temp ^= IsInvertedINPUT_ALARM();
}

bool ButtonStateLIFT_MIN()
{
  temp = mcp.digitalRead(MCP_INPUT_LIFT_MIN);
  return temp ^= IsInvertedLIFT_MIN();
}

bool ButtonStateLIFT_MAX()
{
  temp = mcp.digitalRead(MCP_INPUT_LIFT_MAX);
  return temp ^= IsInvertedLIFT_MAX();
}

bool ButtonStateRAMP_MIN()
{
  temp = mcp.digitalRead(MCP_INPUT_RAMP_MIN);
  return temp ^= IsInvertedRAMP_MIN();
}

bool ButtonStateRAMP_MAX()
{
  temp = mcp.digitalRead(MCP_INPUT_RAMP_MAX);
  return temp ^= IsInvertedRAMP_MAX();
}

bool ButtonStateGATE()
{
  temp = mcp.digitalRead(MCP_INPUT_GATE);
  return temp ^= IsInvertedGATE();
}

void EmergencyStop()
{
  Serial.println("EMERGENCY STOP INITIATED");
  ClearTasks(); // forces current task to stop and task buffer to clear;
  SetIndicatorFlashCount(5);
}

void InputTest()
{
  Serial.print("rampMin: ");
  Serial.print(ButtonStateRAMP_MIN());
  Serial.print("\t");

  Serial.print("rampMax: ");
  Serial.print(ButtonStateRAMP_MAX());
  Serial.print("\t");

  Serial.print("liftMin: ");
  Serial.print(ButtonStateLIFT_MIN());
  Serial.print("\t");

  Serial.print("liftMax: ");
  Serial.print(ButtonStateLIFT_MAX());
  Serial.print("\t");

  Serial.print("gate: ");
  Serial.print(ButtonStateGATE());
  Serial.print("\t");

  Serial.print("up: ");
  Serial.print(ButtonStateINPUT_UP());
  Serial.print("\t");

  Serial.print("down: ");
  Serial.print(ButtonStateINPUT_DOWN());
  Serial.print("\t");

  Serial.print("estop: ");
  Serial.print(ButtonStateEstop());
  Serial.print("\t");

  Serial.print("alarm: ");
  Serial.print(ButtonStateINPUT_ALARM());
  Serial.print("\n");

  Serial.println();

  delay(200);
}

void SetBuzzerActive(bool isOn)
{
  if (isOn)
  {
    mcp.digitalWrite(MCP_PIN_BUZZER, HIGH);
  }
  else
  {
    mcp.digitalWrite(MCP_PIN_BUZZER, LOW);
  }
}

void SetLatchOpen(bool isOn)
{
  isOn ^= IsInvertedLATCH_OUTPUT();
  if (isOn)
  {
    Serial.println("Latch Set LOW");
    mcp.digitalWrite(MCP_PIN_LATCH, LOW);
  }
  else
  {
    Serial.println("Latch Set HIGH");
    mcp.digitalWrite(MCP_PIN_LATCH, HIGH);
  }
}

void SetTestMode(bool isOn)
{
  inTestMode = isOn;
}

void SetIndicatorFlashCount(int count)
{
  flashCount = count;
  currentFlashCount = 0;
  digitalWrite(PIN_STATUS_LED, LOW);
}

void FlashIndicator()
{
  if (currentFlashCount < flashCount)
  {
    if (millis() - lastFlash > shortFlashInterval)
    {
      digitalWrite(PIN_STATUS_LED, !digitalRead(PIN_STATUS_LED));
      lastFlash = millis();

      if (!digitalRead(PIN_STATUS_LED))
      {
        currentFlashCount++;
      }
    }
  }
  else
  {
    if (millis() - lastFlash > longFlashInterval)
    {
      digitalWrite(PIN_STATUS_LED, !digitalRead(PIN_STATUS_LED));
      lastFlash = millis();
      currentFlashCount = 0;
    }
  }
}