#define COMMAND_SIZE 256
#define VERSION "1.0.1"

static char data[COMMAND_SIZE];
static int serial_count;
static int no_data = 0;
static char c;

void InitComms()
{
    Serial.begin(115200);
    ReportID();

    delay(400);
}

void ReportID()
{
    Serial.print("Realrobots Lift Controller v");
    Serial.println(VERSION);
    Serial.println("Ready");
}

void CheckComms()
{
    while (Serial.available() > 0)
    {
        c = Serial.read();
        Serial.print(c);
        if (serial_count < COMMAND_SIZE)
        {
            data[serial_count] = c;
            serial_count++;
        }
    }

    // Incoming packet should be at least one byte plus the EOL \r\n
    if (serial_count > 0)
    {

        if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
        {
            InterpretData();
        }
        ClearData();
    }
    else
    {
        ClearData();
    }
}

void InterpretData()
{
    Serial.println("interpreting");

    if (data[0] == '\r' || data[0] == '\n')
    {
        return;
    }

    if (data[0] == '?')
    {
        Serial.print("Realrobots Lift Controller v");
        Serial.print(VERSION);

        Serial.print("\n");
        Serial.print("Normal operations suspended, unit in test mode.\n");
        Serial.print("$\tPrint Settings\n");
        Serial.print("$n=x\tChange settings (n=index, x=new value)\n");
        Serial.print("d\tRevert To Default Settings\n");
        Serial.print("i\tInput Test\n");
        Serial.print("m\tLift Motor Test\n");
        Serial.print("n\tLift Motor Test\n");
        Serial.print("l\tLatch Test\n");
        Serial.print("b\tBuzzer Test\n");
        Serial.print("q\tReturn to normal mode\n");
        Serial.println();

        SetTestMode(true);
    }
    else if (data[0] == '$')
    {
        if (data[2] == '=' && serial_count >= 6)
        {
            uint8_t idx = data[1] - '0';
            uint8_t val = data[3] - '0';
            ChangeSetting(idx, val);
        }
        else
        {
            PrintSettings();
        }
    }
    else if (data[0] == 'd')
    {
        RevertDefaults();
        Serial.println("Reverted to defaults");
        LoadSettings();
    }        
    else if (data[0] == 'i')
    {
        SetTestMode(true);
        Serial.println("Input test");
        InputTest();
    }
    else if (data[0] == 'm')
    {
        SetTestMode(true);
        Serial.println("Lift Motor Test");
        Motor2Test();
    }
    else if (data[0] == 'n')
    {
        SetTestMode(true);
        Serial.println("Ramp Motor Test");
        Motor1Test();
    }
    else if (data[0] == 'l')
    {
        SetTestMode(true);
        Serial.println("Latch test");
        SetLatchOpen(true);
        delay(1000);
        SetLatchOpen(false);
    }
    else if (data[0] == 'b')
    {
        SetTestMode(true);
        Serial.println("Buzzer test");
        SetBuzzerActive(true);
        delay(500);
        SetBuzzerActive(false);
    }
    else if (data[0] == 'q')
    {
        SetTestMode(false);
        Serial.println("Back To Normal Mode");
    }
}

void ClearData()
{
    for (int i = 0; i < COMMAND_SIZE; i++)
    {
        data[i] = 0;
    }
    serial_count = 0;
}
