#define TASK_WAIT 0
#define TASK_RAISE_RAMP 1
#define TASK_LOWER_RAMP 2
#define TASK_RAISE_LIFT 3
#define TASK_LOWER_LIFT 4
#define TASK_CLOSE_GATE 5 // Waits for gate sensor before activating latch
#define TASK_OPEN_GATE 6

#define ACTION_IDLE 0
#define ACTION_MOVING_UP 1
#define ACTION_MOVING_DOWN 2

uint8_t currentAction = ACTION_IDLE;
uint8_t taskQueue[] = {0, 0, 0, 0, 0, 0, 0, 0};
int currentTask = 0;
bool currentTaskStarted = false;


void DoTasks()
{

    switch (taskQueue[currentTask])
    {
    case TASK_WAIT:

        break;
    case TASK_RAISE_RAMP:
        WhileRaisingRamp();
        break;
    case TASK_LOWER_RAMP:
        WhileLoweringRamp();
        break;
    case TASK_RAISE_LIFT:
        WhileRaisingLift();
        break;
    case TASK_LOWER_LIFT:
        WhileLoweringLift();
        break;
    case TASK_CLOSE_GATE:
        WhileClosingGate();
        break;
    case TASK_OPEN_GATE:
        WhileOpeningGate();
        break;
    }
}

bool IsGoingUp()
{
    return currentAction == ACTION_MOVING_UP;
}

bool IsGoingDown()
{
    return currentAction == ACTION_MOVING_DOWN;
}

bool IsIdle()
{
    return currentAction == ACTION_IDLE;
}

void StartActionGoUp()
{
    Serial.println("Going Up");
    ClearTasks();
    currentAction = ACTION_MOVING_UP;

    taskQueue[0] = TASK_CLOSE_GATE;
    taskQueue[1] = TASK_RAISE_RAMP;
    taskQueue[2] = TASK_RAISE_LIFT;
    //taskQueue[3] = TASK_LOWER_RAMP;
    taskQueue[3] = TASK_OPEN_GATE;

    Serial.println("Start Going Up");
    SetIndicatorFlashCount(2);
}

void StartActionGoDown()
{
    Serial.println("Going Down");
    ClearTasks();
    currentAction = ACTION_MOVING_DOWN;

    taskQueue[0] = TASK_CLOSE_GATE;
    taskQueue[1] = TASK_RAISE_RAMP;
    taskQueue[2] = TASK_LOWER_LIFT;
    taskQueue[3] = TASK_LOWER_RAMP;
    // taskQueue[4] = TASK_OPEN_GATE;

    Serial.println("Start Going Down");
    SetIndicatorFlashCount(3);
}

void OnTaskDone()
{
    currentTaskStarted = false;

    currentTask++;
    if (taskQueue[currentTask] == TASK_WAIT)
    {
        ClearTasks();
    }
    Serial.println("Task Done");
    SetIndicatorFlashCount(1);
}

void ClearTasks()
{
    for (int i = 0; i < 8; i++)
    {
        taskQueue[i] = TASK_WAIT;
    }
    currentTask = 0;
    currentAction = ACTION_IDLE;
    currentTaskStarted = false;
    SetMotorSpeed(1, 0);
    SetMotorSpeed(2, 0);
    motor1_target = 0;
    motor2_target = 0;

    SetIndicatorFlashCount(1);
}

void WhileRaisingRamp()
{
    if (!currentTaskStarted)
    {
        Serial.println("Start Raising Ramp");
        motor1_target = 255;
        currentTaskStarted = true;
    }

    if (ButtonStateRAMP_MAX())
    {
        motor1_target = 0;
    }

    // After target speed set and reached move to next task
    if (motor1_target == 0 && motor1_speed == 0)
    {
        OnTaskDone();
    }
}

void WhileLoweringRamp()
{
    if (!currentTaskStarted)
    {
        Serial.println("Start Lowering Ramp");
        motor1_target = -255;
        currentTaskStarted = true;
    }

    if (ButtonStateRAMP_MIN())
    {
        motor1_target = 0;
    }

    // After target speed set and reached move to next task
    if (motor1_target == 0 && motor1_speed == 0)
    {
        OnTaskDone();
    }
}

void WhileRaisingLift()
{
    if (!currentTaskStarted)
    {
        Serial.println("Start Raising Lift");
        motor2_target = 255;
        currentTaskStarted = true;
    }

    if (ButtonStateLIFT_MAX())
    {
        motor2_target = 0;
    }

    // After target speed set and reached move to next task
    if (motor2_target == 0 && motor2_speed == 0)
    {
        OnTaskDone();
    }
}

void WhileLoweringLift()
{
    if (!currentTaskStarted)
    {
        Serial.println("Start Lowering Lift");
        motor2_target = -255;
        currentTaskStarted = true;
    }

    if (ButtonStateLIFT_MIN())
    {
        motor2_target = 0;
    }

    // After target speed set and reached move to next task
    if (motor2_target == 0 && motor2_speed == 0)
    {
        OnTaskDone();
    }
}

void WhileClosingGate()
{
    if (!currentTaskStarted)
    {
        Serial.println("Start Closing Gate");
        currentTaskStarted = true;
    }

    if (ButtonStateGATE())
    {
        SetLatchOpen(false);
        OnTaskDone();
    }
}

void WhileOpeningGate()
{
    if (!currentTaskStarted)
    {
        Serial.println("Start Opening Gate");
        currentTaskStarted = true;

        SetLatchOpen(true);
        OnTaskDone();
    }
}
