![image-20210120130520480](./front.png)

### SERIAL DEBUGGING
Both NL & CR

115200

| Command | Description |
| --- | --- |
| $ | List Current Settings |
| $x=n | Overwrite setting index *x* with value *n* 0/1 |
| ? | Help Menu *normal function halted in help mode*|
| d | Revert to default settings |
| i | Input Test *lists current input states*|
| m | Lift Motor Test |
| n | Ramp Motor Test |
| l | Latch Test *Opens latch for 1s, then closes* |
| b | Buzzer Test *Sounds buzzer to 0.5s* |
| q | Return to normal mode |

### INDICTOR LIGHT
1 Flashes   Idle

2 Flashes   Moving Up

3 Flashes   Moving Down

5 Flashes   Emergency Stop Activated
### INPUTS

##### BTN UP

Puts the lift into the "up" cycle.

1. Lift waits for gate sensor to be tripped by closed gate
2. Gate latch power cut, locking gate
3. Ramp motor turns clockwise until "Ramp Max" is triggered.
4. Lift motor turns clockwise until "Lift Max" is triggered.
5. Ramp motor turns counter clockwise until "Ramp Min" in triggered.
6. Gate latch power activated, unlocking gate

Pressing while already in cycle does nothing.

Pressing while in down cycle will cancel that cycle and begin an "up" cycle.



##### BTN DOWN

Puts the lift into the "down" cycle.

1. Lift waits for gate sensor to be tripped by closed gate
2. Gate latch power cut, locking gate
3. Ramp motor turns clockwise until "Ramp Max" is triggered.
4. Lift motor turns counter clockwise until "Lift Min" is triggered.
5. Ramp motor turns counter clockwise until "Ramp Min" in triggered.
6. Gate latch power activated, unlocking gate

Pressing while already in cycle does nothing.

Pressing while in up cycle will cancel that cycle and begin an "down" cycle.



##### BTN ESTOP

Immediately cancels the current cycle and power to motors. 



##### BTN ALARM

Activates loud buzzer while held.



##### GATE SENSOR

Limit switch for gate.



##### LIFT MAX

Uppermost limit switch for the lift actuator



##### LIFT MIN

Lowest limit switch for the lift actuator



##### RAMP MAX

Uppermost limit switch for the ramp actuator



##### RAMP MIN

Lowest limit switch for the ramp actuator



### OUTPUTS

##### LATCH POWER

Drives 24v to latch actuator.

##### LIFT MOTOR

24v to lift actuator

##### RAMP MOTOR

24v to lift actuator



### FIRMWARE

Firmware can be updated using a provided USB->UART converter plugged into the "UART Programmer port" and using the Arduino IDE. Set the board to "Arduino Uno".

Latest firmware can be downloaded at https://gitlab.com/jakelwilkinson/liftcontroller/